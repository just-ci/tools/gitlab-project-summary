# GitLab Project Summary

This generates a summary of projects on a GitLab instance with a certain topic.
Can be quite useful if you want people to add repo's to a list, without doing
too much manual work or maintenance. Adds all sorts of useful info surrounding
the project.

## Usage

1. Add to existing CI pipeline in your `.gitlab-ci.yml`.

```yaml
---
include:
  - remote: https://gitlab.com/just-ci/tools/gitlab-project-summary/-/raw/v1.0.7/template.yml
```

2. Add a **protected** CI/CD _File_ variable called `SUMMARY_CONFIG` to your
   GitLab project, which contains a collection of projects or project references
   which need to be added to your summary. Use the following example.

   > This needs to exist in that variable to protect the tokens.

```yaml
https://gitlab.com:
  topics:
    - demo
    - name: tool
      tokens:
        - somereadapitoken
        - anotherperhapsgrouptoken
  projects:
    - 30777660
    - id: 33353393
      token: areadapiaccesstoken
https://gitlab.company.com:
  topics:
    - internal
  projects:
    - id: 333
      token: somereadapiaccesstoken
```

The parent key is the host of the GitLab instance where to find its listed
projects. `topics` is a list of either
[topics](https://docs.gitlab.com/ee/user/project/settings/#topics) which are
accessible by your runner, or key-value pairs of a topic and access tokens which
are used to access projects with that topic. This will also include topics which
are publicly accessible.

For listing specific projects, the structure is the same, where you can specify
publicly accessible projects, or provide an access token for that specific
project.

This runs in the `.pre` stage, and will make an artifact available to subsequent
stages called `summaries/gitlab-project-summary.md` by default. This can be
changed using the `SUMMARY_OUTPUT_FILENAME` variable.

> This is meant to be used in an existing pipeline. If you just want this job to
> run ensure you set the stage of the `summary` job to something other than
> `.pre`.

3. (Optional) If you'd like your readers to be able to refresh the job on
   demand, create a
   [Pipeline trigger token](https://docs.gitlab.com/ee/ci/triggers/#create-a-trigger-token)
   , and add it as a **protected** CI/CD variable called
   `SUMMARY_REFRESH_TOKEN`.
