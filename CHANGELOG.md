## [1.0.7](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.6...v1.0.7) (2023-02-14)


### Bug Fixes

* cr replace in table ([ca2b9c7](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/ca2b9c7ca1e66b2d667d591a854c4b7f566f0b80))

## [1.0.6](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.5...v1.0.6) (2022-09-27)


### Bug Fixes

* comment topic ([feeaba7](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/feeaba7a5cb84ce5e1ad0d28ea2751fb02c53a6b))

## [1.0.5](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.4...v1.0.5) (2022-09-05)


### Bug Fixes

* retrieving topics without tokens ([952846c](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/952846c6aae14db45ed1bb1308e868936b5a0fa1))

## [1.0.4](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.3...v1.0.4) (2022-09-01)


### Bug Fixes

* retrieve project details with correct token ([7259890](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/72598903ad2c8f371dd960202bf75a88ecec8dce))

## [1.0.3](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.2...v1.0.3) (2022-08-31)


### Bug Fixes

* run only on default branch ([fd8aafd](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/fd8aafd6b72fa789a05d06961bda22d9609bd235))

## [1.0.2](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.1...v1.0.2) (2022-08-31)


### Bug Fixes

* require config ([e1e1231](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/e1e123140a1abc95519a3b4b4319dff4386f64f0))

## [1.0.1](https://gitlab.com/just-ci/tools/gitlab-project-summary/compare/v1.0.0...v1.0.1) (2022-08-31)


### Bug Fixes

* reference to new path ([333e389](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/333e389a4af473c43aabbc747e6bd4eaa1e9784d))

# 1.0.0 (2022-08-31)


### Bug Fixes

* output path ([c7c4b50](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/c7c4b50f0280845de7d9659717afeb686eb4e585))
* pin ci ([7063be7](https://gitlab.com/just-ci/tools/gitlab-project-summary/commit/7063be758ee661b2fc033327f4cdc24cb4b77cde))
