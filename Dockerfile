ARG PYVERSION=3.11-slim
FROM python:$PYVERSION

WORKDIR /app

COPY . .

RUN pip install .
