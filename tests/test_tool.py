from gitlab_project_summary.__main__ import parse_args
from gitlab_project_summary.main import main


def test_config() -> None:
    main(
        parse_args(
            [
                "--config",
                "tests/projects.yaml",
                "--output",
                "output/config.md",
                "--verbose",
            ]
        )
    )


def test_config_comment() -> None:
    main(
        parse_args(
            [
                "--config",
                "tests/projects.yaml",
                "--force-comment",
                "--output",
                "output/comment.md",
                "--verbose",
            ]
        )
    )


def test_config_print() -> None:
    main(
        parse_args(
            [
                "--config",
                "tests/projects.yaml",
                "--print",
                "--output",
                "output/print.md",
                "--verbose",
            ]
        )
    )
