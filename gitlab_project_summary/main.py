import logging
import os
from datetime import datetime
from typing import Dict, List, Optional

import configargparse
import pytz
import yaml
from gitlab import Gitlab
from gitlab.exceptions import (
    GitlabAuthenticationError,
    GitlabHttpError,
    GitlabListError,
)
from tomark import Tomark

from gitlab_project_summary.utils.misc import pretty_time_delta_dm

logger = logging.getLogger(__name__)


def generate_comment(topics: List[str], refresh_token: Optional[str]) -> str:
    comment = (
        "> <small>This list is generated "
        "[automatically](https://gitlab.com/just-ci/tools/gitlab-project-summary) "
        "and sorted descending on last commit date."
    )
    if topics:
        comment += (
            " Want to add your project? Add the "
            "[topic](https://docs.gitlab.com/ee/user/project/settings/#topics) "
            f"`{'` or `'.join(topics)}` to your "
            "project, and make sure the project is either visible as Public, or a "
            "`read_api` scoped Project Access Token for your project is added to the "
            "job. Send that token to the maintainer of this page. It will be listed "
            "here after the next Pipeline run"
        )

        if refresh_token and "CI" in os.environ:
            comment += (
                ", which you can trigger yourself using:\n\n"
                "```shell\n"
                f"curl -X POST -F token={refresh_token} -F ref={os.environ['CI_DEFAULT_BRANCH']} {os.environ['CI_API_V4_URL']}/projects/{os.environ['CI_PROJECT_ID']}/trigger/pipeline\n"
                "```\n"
            )
        else:
            comment += "."
    comment += "</small>"
    return comment


def main(args: configargparse.Namespace) -> None:
    logger.info(f"Using config file {args.config}.")
    logger.debug("Debug logging enabled.")

    project_summaries: Dict[int, Dict[str, str]] = dict()

    with open(args.config, "r", encoding="utf-8") as file_stream:
        config = yaml.safe_load(file_stream.read())

    for host, _ in config.items():
        if "topics" in config[host]:
            for topic in config[host]["topics"]:
                if isinstance(topic, dict):
                    logger.debug(
                        f"[*] Retrieving summaries from {host} with topic {topic['name']} using tokens..."
                    )
                    project_summaries |= get_topic_project_summaries(
                        host, topic["name"], topic["tokens"]
                    )
                else:
                    logger.debug(
                        f"[*] Retrieving summaries from {host} with topic {topic}..."
                    )
                    project_summaries |= get_topic_project_summaries(host, topic)
        if "projects" in config[host]:
            for project in config[host]["projects"]:
                if isinstance(project, dict):
                    gl = Gitlab(host, private_token=project["token"])
                    if summary := get_project_summary(gl, project["id"]):
                        project_summaries |= {int(project["id"]): summary}
                else:
                    gl = Gitlab(host)
                    if summary := get_project_summary(gl, project):
                        project_summaries |= {project: summary}

    project_summaries = dict(
        sorted(
            project_summaries.items(),
            key=lambda x: x[1].pop("temp_sort"),
            reverse=True,
        )
    )

    project_summary_list = list()
    for _, project in project_summaries.items():
        project_summary_list.append(project)

    logger.info(f"Found a total of {len(project_summary_list)} unique project(s).")
    if len(project_summary_list) != 0:
        markdown = Tomark.table(project_summary_list)
    else:
        markdown = "No projects found :("

    if (
        ci_server_url := os.getenv("CI_SERVER_URL")
    ) in config.keys() or args.force_comment:
        logger.debug(f"Found our CI server URL ({ci_server_url}) in config.")
        topics = []
        for host, _ in config.items():
            if "topics" in config[host] and host == ci_server_url:
                for topic in config[host]["topics"]:
                    if isinstance(topic, dict):
                        topics.append(topic["name"])
                    else:
                        topics.append(topic)
        markdown += "\n" + generate_comment(topics, args.refresh_token)

    if args.print:
        print(markdown)

    with open(args.output, "w", encoding="utf-8") as file_stream:
        file_stream.write(markdown)
        logger.info(f"[+] Output written to {args.output}.")


def get_project_summary(gl: Gitlab, project_id: int) -> Dict[str, str]:
    logger.debug(f"Retrieving data for project ID {project_id}.")
    try:
        project = gl.projects.get(project_id, simple=False)
    except GitlabAuthenticationError:
        logger.warning(
            f"[!] Could not authenticate with project ID {project_id}. Skipping."
        )
        return dict()
    except GitlabHttpError:
        logger.warning(f"[!] Could not retrieve project ID {project_id}. Skipping.")
        return dict()
    except Exception:
        logger.warning(
            f"[!] Something went wrong retrieving project ID {project_id}. Skipping."
        )
        return dict()

    try:
        if project.visibility == "public":
            visibility = "🌐"
        elif project.visibility == "internal":
            visibility = "🛡"
        else:
            visibility = "🔒"
    except AttributeError:  # Unauthenticated requests do not provide this attribute
        visibility = "🌐"

    created_datetime = datetime.strptime(project.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    created_datetime_localized = pytz.utc.localize(created_datetime)

    # Replace breaking characters
    description = (
        project.description.replace("|", "\\|")
        .replace("\r\n", "<br>")
        .replace("\n", "<br>")
        .replace("\r", "<br>")
    )

    project_summary = {
        "Title": f"[{project.name}]({project.web_url})&nbsp;{visibility}",
        "Description": description,
    }

    try:
        last_commit = list(project.commits.list(all=False))[0]
        last_commit_datetime = datetime.strptime(
            last_commit.created_at, "%Y-%m-%dT%H:%M:%S.%f%z"
        )

        if (
            created_datetime.year == last_commit_datetime.year
            and created_datetime.month == last_commit_datetime.month
        ):
            period = f"{created_datetime.year}&#8209;{created_datetime.month}"
        else:
            period = f"{created_datetime.year}&#8209;{created_datetime.month}&nbsp;until&nbsp;{last_commit_datetime.year}&#8209;{last_commit_datetime.month}"

        if duration := pretty_time_delta_dm(
            int((last_commit_datetime - created_datetime_localized).total_seconds())
        ):
            period += f"<br>({duration})"

        project_summary["Activity"] = f"{period}"
        project_summary["temp_sort"] = last_commit_datetime.isoformat()
    except (GitlabListError, IndexError):
        logger.warning(
            f"[!] Could not retrieve commit activity details of project ID {project_id}."
        )
        project_summary["temp_sort"] = datetime.fromtimestamp(0).isoformat()

    # members = []  # TODO

    return project_summary


def get_topic_project_summaries(
    host: str, topic: str, tokens: str = ""
) -> Dict[int, Dict[str, str]]:
    project_summaries: Dict[int, Dict[str, str]] = dict()

    if tokens:
        for token in tokens:
            try:
                gl = Gitlab(host, private_token=token)
                new_projects = gl.projects.list(all=True, topic=topic)
                for project in new_projects:
                    if summary := get_project_summary(gl, project.id):
                        project_summaries[project.id] = summary
                logger.info(
                    f"[+] Found {len(new_projects)} project(s) for the token starting with '{token[:3]}'"
                )
            except GitlabAuthenticationError:
                logger.warning(
                    f"[!] The token starting with '{token[:3]}' could not authenticate. Skipping."
                )
                continue
            except GitlabListError:
                logger.warning(
                    f"[!] The token starting with '{token[:3]}' does not have the right scope. "
                    "Ensure it has the 'read_api' scope. Skipping."
                )
                continue
    else:
        try:
            gl = Gitlab(host)
            new_projects = gl.projects.list(all=True, topic=topic)
            for project in new_projects:
                if summary := get_project_summary(gl, project.id):
                    project_summaries[project.id] = summary
            logger.info(f"[+] Found {len(new_projects)} project(s)")
        except Exception:
            logger.warning("[!] Something went wrong.", exc_info=True)

    return project_summaries
