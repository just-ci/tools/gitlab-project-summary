import logging
import sys
from pathlib import Path
from typing import List

import configargparse
import yaml

from gitlab_project_summary.main import main


def parse_args(argv: List[str] = sys.argv[1:]) -> configargparse.Namespace:
    parser = configargparse.ArgumentParser()

    def existing_file(string: str) -> Path:
        path = Path(string).absolute()
        if not path.is_file():
            parser.error(f"{string} does not exist or is not a file.")
        return path

    def nonexisting_path(string: str) -> Path:
        path = Path(string).absolute()
        if path.exists():
            parser.error(f"{string} already exists.")
        return path

    def counter_path(string: str) -> Path:
        # Just keep adding ones.
        path = Path(string).absolute()
        path.parent.mkdir(parents=True, exist_ok=True)
        while path.exists():
            path = path.parent / (path.stem + ".1" + path.suffix)
        return path

    def valid_yaml(string: str) -> Path:
        with open(path := existing_file(string), "r", encoding="utf-8") as file_stream:
            try:
                yaml.safe_load(file_stream.read())
            except yaml.YAMLError:
                parser.error("Unable to load YAML file.")
        return path

    parser.add_argument(
        "--config",
        help="Config with all topics and projects to retrieve.",
        type=valid_yaml,
        env_var="SUMMARY_CONFIG",
        required=True,
    )
    parser.add_argument(
        "--output",
        help="Where to write the output markdown.",
        type=counter_path,
        default="gitlab-project-summary.md",
        env_var="SUMMARY_OUTPUT_FILENAME",
    )
    parser.add_argument("--print", help="Print results to stdout.", action="store_true")
    parser.add_argument(
        "--refresh-token",
        help="A refresh token for your pipeline. See https://docs.gitlab.com/ee/ci/triggers/#create-a-trigger-token",
        type=str,
        env_var="SUMMARY_REFRESH_TOKEN",
    )
    parser.add_argument(
        "--force-comment", help="Force generating comment.", action="store_true"
    )
    parser.add_argument(
        "-v", "--verbose", help="Print debug messages.", action="store_true"
    )
    parsed_args = parser.parse_args(argv)

    return parsed_args


if __name__ == "__main__":
    args = parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    main(args)
