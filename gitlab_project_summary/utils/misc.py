def pretty_time_delta(seconds: int) -> str:
    years, seconds = divmod(seconds, 31536000)
    months, seconds = divmod(seconds, 2629800)
    weeks, seconds = divmod(seconds, 604800)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if years > 0:
        return f"{years:.0f}y{months:.0f}m{weeks:.0f}w{days:.0f}d{hours:.0f}h{minutes:.0f}m{seconds:.0f}s"
    elif months > 0:
        return f"{months:.0f}m{weeks:.0f}w{days:.0f}d{hours:.0f}h{minutes:.0f}m{seconds:.0f}s"
    elif weeks > 0:
        return f"{weeks:.0f}w{days:.0f}d{hours:.0f}h{minutes:.0f}m{seconds:.0f}s"
    elif days > 0:
        return f"{days:.0f}d{hours:.0f}h{minutes:.0f}m{seconds:.0f}s"
    elif hours > 0:
        return f"{hours:.0f}h{minutes:.0f}m{seconds:.0f}s"
    elif minutes > 0:
        return f"{minutes:.0f}m{seconds:.0f}s"
    else:
        return f"{seconds:.0f}s"


def pretty_time_delta_dm(seconds: int) -> str:
    result = ""
    months, _ = divmod(seconds, 2629800)
    weeks, _ = divmod(seconds, 604800)
    days, _ = divmod(seconds, 86400)
    if months > 0:
        result = f"{months:.0f} month"
        if months != 1:
            result += "s"
    elif weeks > 0:
        result = f"{weeks:.0f} week"
        if weeks != 1:
            result += "s"
    elif days > 0:
        result = f"{days:.0f} day"
        if days != 1:
            result += "s"
    return result
